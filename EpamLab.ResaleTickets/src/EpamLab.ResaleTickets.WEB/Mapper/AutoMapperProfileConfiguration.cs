﻿using EpamLab.ResaleTickets.Business.Models;
using EpamLab.ResaleTickets.DataAccess.Entries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EpamLab.ResaleTickets.WEB.Models;

namespace EpamLab.ResaleTickets.WEB.Mapper
{
    public class AutoMapperProfileConfiguration : Profile
    {
        [Obsolete]
        protected override void Configure()
        {
            CreateMap<City, CityBusinessModel>();
            CreateMap<Event, EventBusinessModel>();
            CreateMap<User, UserBusinessModel>();
            CreateMap<Venue, VenueBusinessModel>();
            CreateMap<Order, OrderBusinessModel>();
            CreateMap<Ticket,TicketBusinessModel>();
            CreateMap<Status, StatusBusinessModel>();
            CreateMap<UserBusinessModel, UserModelView>();
            CreateMap<EventBusinessModel, EventModelView>();
            CreateMap<OrderBusinessModel, OrderModelView>();
            CreateMap<TicketBusinessModel, TicketModelView>();
            CreateMap<StatusBusinessModel, StatusModelEnum>();
        }
    }
}
