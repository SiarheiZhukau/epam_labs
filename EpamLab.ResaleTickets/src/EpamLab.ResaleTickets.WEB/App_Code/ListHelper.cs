﻿using EpamLab.ResaleTickets.WEB.Controllers;
using EpamLab.ResaleTickets.WEB.Models;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Text;

namespace EpamLab.ResaleTickets.WEB.App_Code
{
    public static class ListHelper
    {
        public static HtmlString CreateTicketRows(this IHtmlHelper html, string title, IEnumerable<TicketModelView> tickets, IHtmlLocalizer Localizer)
        {
            StringBuilder result = new StringBuilder();
            result.Append($"<h1>{@Localizer[title].Value}</h1>");
            result.Append("<table align=\"center\">");
            result.Append("<tr>");
            result.Append($"<th>{@Localizer["Poster"].Value}</th>");
            result.Append($"<th>{@Localizer["EventName"].Value}</th>");
            result.Append($"<th>{@Localizer["Date"].Value}</th>");
            result.Append($"<th>{@Localizer["Price"].Value}</th>");
            result.Append($"<th>{@Localizer["Address"].Value}</th>");
            result.Append($"<th>{@Localizer["SellerNote"].Value}</th>");
            result.Append($"</tr>");
            foreach (TicketModelView e in tickets)
            {
                result.Append("<tr>");
                result.Append($"<td><img src=\"/{Constant.PosterPath}{e.Event.Poster}\" height=\"100\"></td>");
                result.Append($"<td>{e.Event.Name}</td>");
                result.Append($"<td>{e.Event.Date[0].Date}</td>");
                result.Append($"<td>{e.Price}</td>");
                result.Append($"<td>{e.Event.Venue}, {@e.Event.City}</td>");
                result.Append($"<td>{e.Note}</td>");
                result.Append("</tr>");
            }
            result.Append("</table>");
            return new HtmlString(result.ToString());
        }
    }
}
