﻿using EpamLab.ResaleTickets.Business.Models;
using System.Collections.Generic;

namespace EpamLab.ResaleTickets.WEB.Models
{
    public class OrderModelView
    {
        public int Id { get; set; }
        public StatusModelEnum Status { get; set; }
        public TicketModelView TicketSell { get; set; }
        public UserModelView Buyer { get; set; }
        public string TrackNumber { get; set; }

        public static OrderModelView Get(DataAccess.Entries.Order order)
        {
            OrderModelView result = new OrderModelView() { Id = order.Id, Status = (StatusModelEnum)(order.Status), TicketSell = TicketModelView.Get(order.Ticket), Buyer = UserModelView.Get(order.Buyer), TrackNumber = order.TrackNumber };
            return result;
        }
        public static IEnumerable<OrderModelView> Get(IEnumerable<DataAccess.Entries.Order> orders)
        {
            List<OrderModelView> result = new List<OrderModelView>();
            foreach (DataAccess.Entries.Order order in orders)
            {
                result.Add(OrderModelView.Get(order));
            }
            return result;
        }
        public static OrderModelView Get(OrderBusinessModel order)
        {
            OrderModelView result = new OrderModelView() { Id = order.Id, Status = (StatusModelEnum)(order.Status), TicketSell = TicketModelView.Get(order.Ticket), Buyer = UserModelView.Get(order.Buyer), TrackNumber = order.TrackNumber };
            return result;
        }
        public static IEnumerable<OrderModelView> Get(IEnumerable<OrderBusinessModel> orders)
        {
            List<OrderModelView> result = new List<OrderModelView>();
            foreach (OrderBusinessModel order in orders)
            {
                result.Add(OrderModelView.Get(order));
            }
            return result;
        }
    }
}
