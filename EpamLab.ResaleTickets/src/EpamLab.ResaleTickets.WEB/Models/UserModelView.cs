﻿using EpamLab.ResaleTickets.Business.Models;

namespace EpamLab.ResaleTickets.WEB.Models
{
    public class UserModelView
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Localization { get; set; }
        public string Adress { get; set; }
        public string PhoneNumber { get; set; }
        public string Login { get; set; }


        public static UserModelView Get(DataAccess.Entries.User user)
        {
            UserModelView result = new UserModelView() { Id = user.Id, FirstName = user.FirstName, LastName = user.LastName, Adress = user.Adress, Localization = user.Localization, Login = user.Login, PhoneNumber = user.PhoneNumber };
            return result;
        }

        public static UserModelView Get(UserBusinessModel user)
        {
            UserModelView result = new UserModelView() { Id = user.Id, FirstName = user.FirstName, LastName = user.LastName, Adress = user.Adress, Localization = user.Localization, Login = user.Login, PhoneNumber = user.PhoneNumber };
            return result;
        }


    }
}
