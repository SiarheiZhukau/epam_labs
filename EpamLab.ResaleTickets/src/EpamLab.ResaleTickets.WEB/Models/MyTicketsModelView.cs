﻿using System.Collections.Generic;

namespace EpamLab.ResaleTickets.WEB.Models
{
    public class MyTicketsModelView
    {
        public IEnumerable<TicketModelView> Selling { get; set; }
        public IEnumerable<TicketModelView> Waiting { get; set; }
        public IEnumerable<TicketModelView> Sold { get; set; }

        public static IEnumerable<TicketModelView> OrderMVToTicketsMV(IEnumerable<OrderModelView> orders)
        {
            List<TicketModelView> tickets = new List<TicketModelView>();
            foreach (OrderModelView o in orders)
            {
                tickets.Add(o.TicketSell);
            }
            return tickets;
        }
    }
}
