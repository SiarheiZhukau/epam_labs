﻿using EpamLab.ResaleTickets.Business.Models;
using System.Collections.Generic;

namespace EpamLab.ResaleTickets.WEB.Models
{
    public class TicketModelView
    {
        public TicketModelView()
        { }

        public TicketModelView(TicketModelView ticket)
        {
            Id = ticket.Id;
            SelerName = SelerName;
            SelerId = SelerId;
            price = ticket.price;
            Event = ticket.Event;
            Note = ticket.Note;
            

        }
        //: Seller Name, Price, Seller Notes
        public int Id { get; set; }
        public string SelerName { get; set; }
        public int SelerId { get; set; }
        public decimal price;
        public string Price
        {
            get { return ((double)price / 100).ToString(); }
        }
        public EventModelView Event;
        public string Note { get; set; }
        public static TicketModelView Get(DataAccess.Entries.Ticket ticket)
        {
            TicketModelView result = new TicketModelView();
            result.Id = ticket.Id;
            result.SelerId = ticket.Seller.Id;
            result.price = ticket.Price;
            result.Note = ticket.Note;
            result.SelerName = $"{ticket.Seller.FirstName} {ticket.Seller.LastName}";
            result.Event = EventModelView.Get(ticket.Event);
            return result;
        }
        public static IEnumerable<TicketModelView> Get(IEnumerable<DataAccess.Entries.Ticket> tickets)
        {
            List<TicketModelView> result = new List<TicketModelView>();
            foreach( DataAccess.Entries.Ticket t in tickets)
            {
                result.Add(TicketModelView.Get(t));
            }
            return result;
        }

        public static TicketModelView Get(TicketBusinessModel ticket)
        {
            TicketModelView result = new TicketModelView();
            result.Id = ticket.Id;
            result.SelerId = ticket.Seller.Id;
            result.price = ticket.Price;
            result.Note = ticket.Note;
            result.SelerName = $"{ticket.Seller.FirstName} {ticket.Seller.LastName}";
            result.Event = EventModelView.Get(ticket.Event);
            return result;
        }
        public static IEnumerable<TicketModelView> Get(IEnumerable<TicketBusinessModel> tickets)
        {
            List<TicketModelView> result = new List<TicketModelView>();
            foreach (TicketBusinessModel t in tickets)
            {
                result.Add(TicketModelView.Get(t));
            }
            return result;
        }

    }
}
