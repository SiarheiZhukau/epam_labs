﻿using EpamLab.ResaleTickets.Business.Models;
using System;
using System.Collections.Generic;

namespace EpamLab.ResaleTickets.WEB.Models
{
    public class EventModelView
    {
        //Image, Name, Date, City, Venue.
        public int Id { get; set; }
        public string Poster { get; set; }
        public string Name { get; set; }
        public List<IdDate> Date { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Venue { get; set; }
        public string Description { get; set; }

        //преобразуем событие во вьюшку
        public static EventModelView Get(EpamLab.ResaleTickets.DataAccess.Entries.Event ev)
        {
            EventModelView result = new EventModelView() { Poster = ev.Banner, City = ev.Venue.City.Name, Id = ev.Id, Name = ev.Name, Venue = ev.Venue.Name, Address = ev.Venue.Address, Description = ev.Description };
            result.Date = new List<IdDate>();
            result.Date.Add(new IdDate() { Date = ev.Date, Id = ev.Id });
            return result;
        }

        //преобразуем колекцию событий к виду одно событие много дат для вьюшки
        public static Dictionary<string, EventModelView> Get(IEnumerable<DataAccess.Entries.Event> ev)
        {
            Dictionary<string, EventModelView> result = new Dictionary<string, EventModelView>();
            foreach (DataAccess.Entries.Event e in ev)
            {
                if (result.ContainsKey(e.Name))
                {
                    result[e.Name].Date.Add(new IdDate() { Date = e.Date, Id = e.Id });
                }
                else
                {
                    result.Add(e.Name, EventModelView.Get(e));
                }
            }

            return result;
        }

        //преобразуем событие во вьюшку
        public static EventModelView Get(EventBusinessModel ev)
        {
            EventModelView result = new EventModelView() { Poster = ev.Banner, City = ev.Venue.City.Name, Id = ev.Id, Name = ev.Name, Venue = ev.Venue.Name, Address = ev.Venue.Address, Description = ev.Description };
            result.Date = new List<IdDate>();
            result.Date.Add(new IdDate() { Date = ev.Date, Id = ev.Id });
            return result;
        }

        //преобразуем колекцию событий к виду одно событие много дат для вьюшки
        public static Dictionary<string, EventModelView> Get(IEnumerable<EventBusinessModel> ev)
        {
            Dictionary<string, EventModelView> result = new Dictionary<string, EventModelView>();
            foreach (EventBusinessModel e in ev)
            {
                if (result.ContainsKey(e.Name))
                {
                    result[e.Name].Date.Add(new IdDate() { Date = e.Date, Id = e.Id });
                }
                else
                {
                    result.Add(e.Name, EventModelView.Get(e));
                }
            }

            return result;
        }
    }

    //Класс для хранения дата - ид для получения ссылки на нужный меропритие
    public class IdDate
    {
        public DateTime Date { get; set; }
        public int Id { get; set; }
    }
}

