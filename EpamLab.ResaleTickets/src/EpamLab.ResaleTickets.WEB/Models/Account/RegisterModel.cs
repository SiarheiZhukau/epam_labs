﻿using System.ComponentModel.DataAnnotations;

namespace EpamLab.ResaleTickets.WEB.Models.Account
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Не указан логин")]
        public string Login { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage ="Не указан пароль")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Не указано имя")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Не указана фамилия")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Не указан Адрес")]
        public string Address { get; set; }

        [DataType (DataType.PhoneNumber)]
        [Required(ErrorMessage = "Не указан телефон")]
        public string PhoneNumber { get; set; }
    }
    
}
