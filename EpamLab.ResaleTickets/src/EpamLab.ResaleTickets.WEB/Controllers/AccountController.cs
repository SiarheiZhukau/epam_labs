﻿using Microsoft.AspNetCore.Mvc;
using EpamLab.ResaleTickets.WEB.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EpamLab.ResaleTickets.DataAccess.Interfaces;
using EpamLab.ResaleTickets.DataAccess.Entries;
using System.Security.Claims;
using EpamLab.ResaleTickets.WEB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http;
using EpamLab.ResaleTickets.Business.Interfaces;
using Microsoft.AspNetCore.Identity;
using EpamLab.ResaleTickets.Business.Models;

namespace EpamLab.ResaleTickets.WEB.Controllers
{
    public class AccountController : Controller
    {
        private IUnitOfWork uow;
        private IUserService userService;
        private readonly IStringLocalizer<HomeController> localizer;
        public AccountController(IUserService userService, IUnitOfWork uow, IStringLocalizer<HomeController> localizer)
        {
            this.uow = uow;
            this.localizer = localizer;
            this.userService = userService;
            
        }
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            var uu = User;
            if (ModelState.IsValid)
            {
               var normalizer = new UpperInvariantLookupNormalizer();
               UserBusinessModel user = userService.Aunthefication(normalizer.Normalize(model.Login), DataAccess.PasswordHasher.Hash(model.Password));
                if (user != null)
                {
                    await Authenticate(user); // аутентификация

                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                User user = uow.Users.Find(u => u.Login == model.Login).First();
                if (user == null)
                {
                    // добавляем пользователя в бд
                    uow.Users.Create(new User { Login = model.Login, Password = model.Password, Adress=model.Address,FirstName=model.FirstName,
                        LastName =model.LastName,PhoneNumber=model.PhoneNumber, Role = Role.User });
                    

                   // await Authenticate(user); // аутентификация

                    return RedirectToAction("Index", "Home");
                }
                else
                    ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }

        private async Task Authenticate(UserBusinessModel user)
        {
            // создаем один claim
            var claims = new List<Claim>
                    {
                        new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login),
                        new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role.ToString().ToLower()),
                        new Claim("userName",$"{user.FirstName} {user.LastName}"),
                        new Claim("userId", user.Id.ToString())    
                    };
            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await HttpContext.Authentication.SignInAsync("Cookies", new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync("Cookies");
            return RedirectToAction("Login", "Account");
        }

        
        [Authorize]
        public IActionResult UserInfo()
        {
            UserModelView u = UserModelView.Get(uow.Users.Find(v => v.Login == User.Identity.Name).First());
            return View(u);
        }
        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );
            return LocalRedirect(returnUrl);
        }
    }
}
