﻿using EpamLab.ResaleTickets.Business.Interfaces;
using EpamLab.ResaleTickets.WEB.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System.Collections.Generic;

namespace EpamLab.ResaleTickets.WEB.Controllers
{
    public class HomeController : Controller
    {
        private IEventService eventService;
        private ITicketService ticketService;
        private readonly IStringLocalizer<HomeController> localizer;
        public HomeController(IEventService eventService, ITicketService ticketService, IStringLocalizer<HomeController> localizer)
        {
            this.eventService = eventService;
            this.ticketService = ticketService;
            this.localizer = localizer;
        }

        public IActionResult Index()
        {
            Dictionary<string, EventModelView> events = EventModelView.Get(eventService.GetActual());
            ViewBag.Keys = events.Keys;
            return View(events);
        }
        [HttpGet]
        public IActionResult Tickets(int eventId)
        {
            IEnumerable<TicketModelView> t = TicketModelView.Get(ticketService.GetForEvent(eventId));
            ViewBag.ev = EventModelView.Get(eventService.Get(eventId));
            
            return View(t);
        }
    }
}
