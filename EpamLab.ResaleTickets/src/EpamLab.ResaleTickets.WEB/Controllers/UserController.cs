﻿using EpamLab.ResaleTickets.Business.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using EpamLab.ResaleTickets.WEB.Models;

namespace EpamLab.ResaleTickets.WEB.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private ITicketService ticketService;
        private IOrderService orderService;
        private readonly IStringLocalizer<UserController> localizer;
        public UserController(IOrderService orderService, ITicketService ticketService, IStringLocalizer<UserController> localizer)
        {
            this.orderService = orderService;
            this.ticketService = ticketService;
               this.localizer = localizer;
        }
        [Authorize]
        public IActionResult Orders()
        {
            return View(OrderModelView.Get(orderService.GetBuyer(User.Identity.Name)));
        }
        [Authorize]
        public IActionResult Tickets()
        {
            int userId = int.Parse(User.FindFirst(ue => ue.Type == "userId").Value);
            MyTicketsModelView model = new MyTicketsModelView();
            model.Selling = TicketModelView.Get(ticketService.GetFromUser(userId));
            model.Waiting = TicketModelView.Get(ticketService.GetWaitingFromUser(userId));
            model.Sold = TicketModelView.Get(ticketService.GetSoldFromUser(userId));
            return View(model);
        }
    }
}
