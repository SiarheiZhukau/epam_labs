﻿using EpamLab.ResaleTickets.DataAccess.Interfaces;
using System;
using EpamLab.ResaleTickets.DataAccess.Entries;

namespace EpamLab.ResaleTickets.DataAccess.Repositories.TestMemory
{
    public class MemoryTestData : IUnitOfWork
    {
        private MemoryTestRepository<City> cities;
        private MemoryTestRepository<Order> orders;
        private MemoryTestRepository<Event> events;
        private MemoryTestRepository<Ticket> tickets;
        private MemoryTestRepository<Venue> venues;
        private MemoryTestRepository<User> users;

        public MemoryTestData()
        {
            cities = new MemoryTestRepository<City>();
            orders = new MemoryTestRepository<Order>();
            events = new MemoryTestRepository<Event>();
            tickets = new MemoryTestRepository<Ticket>();
            venues = new MemoryTestRepository<Venue>();
            users = new MemoryTestRepository<User>();
        }
        public IRepository<City> Cities
        {
            get
            {
                if (cities == null)
                    cities = new MemoryTestRepository<City>();
                return cities;
            }
        }

        public IRepository<Event> Events
        {
            get
            {
                if (events == null)
                    events = new MemoryTestRepository<Event>();
                return events;
            }
        }

        public IRepository<Order> Orders
        {
            get
            {
                if (orders == null)
                    orders = new MemoryTestRepository<Order>();
                return orders;
            }
        }

        public IRepository<Ticket> Tickets
        {
            get
            {
                if (tickets == null)
                    tickets = new MemoryTestRepository<Ticket>();
                return tickets;
            }
        }

        public IRepository<User> Users
        {
            get
            {
                if (users == null)
                    users = new MemoryTestRepository<User>();
                return users;
            }
        }

        public IRepository<Venue> Venues
        {
            get
            {
                if (venues == null)
                    venues = new MemoryTestRepository<Venue>();
                return venues;
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}
