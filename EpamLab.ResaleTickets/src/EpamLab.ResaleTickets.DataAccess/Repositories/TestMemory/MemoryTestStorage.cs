﻿using EpamLab.ResaleTickets.DataAccess.Entries;
using System;
using Microsoft.AspNetCore.Identity;

namespace EpamLab.ResaleTickets.DataAccess.Repositories.TestMemory
{
    public static class MemoryTestStorage
    {
        private static MemoryTestRepository<City> cities;
        private static MemoryTestRepository<Order> orders;
        private static MemoryTestRepository<Event> events;
        private static MemoryTestRepository<Ticket> tickets;
        private static MemoryTestRepository<Venue> venues;
        private static MemoryTestRepository<User> users;

        public static MemoryTestRepository<City> Cities
        {
            get
            {
                if (cities == null)
                {
                    cities = new MemoryTestRepository<City>();
                    AddCities();
                };

                return cities;
            }
        }
        public static MemoryTestRepository<Order> Orders
        {
            get
            {
                if (orders == null)
                {
                    orders = new MemoryTestRepository<Order>();
                    AddOrders();
                };

                return orders;
            }
        }
        public static MemoryTestRepository<Event> Events
        {
            get
            {
                if (events == null)
                {
                    events = new MemoryTestRepository<Event>();
                    AddEvents();
                };

                return events;
            }
        }
        public static MemoryTestRepository<Ticket> Tickets
        {
            get
            {
                if (tickets == null)
                {
                    tickets = new MemoryTestRepository<Ticket>();
                    AddTickets();
                };

                return tickets;
            }
        }
        public static MemoryTestRepository<Venue> Venues
        {
            get
            {
                if (venues == null)
                {
                    venues = new MemoryTestRepository<Venue>();
                    AddVenues();
                };

                return venues;
            }
        }
        public static MemoryTestRepository<User> Users
        {
            get
            {
                if (users == null)
                {
                    users = new MemoryTestRepository<User>();
                    AddUsers();
                };

                return users;
            }
        }


        private static void AddCities()
        {
            cities.Create(new City { Id = 1, Name = "Minsk" });
            cities.Create(new City { Id = 2, Name = "Mogilev" });
            cities.Create(new City { Id = 3, Name = "Grodno" });
            cities.Create(new City { Id = 4, Name = "Vitebsk" });
            cities.Create(new City { Id = 5, Name = "Brest" });
            cities.Create(new City { Id = 6, Name = "Gomel" });
        }
        private static void AddVenues()
        {
            venues.Create(new Venue { Id = 1, City = cities.Get(1), Name = "Друзья", Address = "ул. Кульман, 40" });
            venues.Create(new Venue { Id = 2, City = cities.Get(1), Name = "Белорусская государственная филармония", Address = "пр-т Независимости, 50" });

            venues.Create(new Venue { Id = 3, City = cities.Get(2), Name = "Кинотеатр Космос", Address = "пр-т Пушкинский, 10" });
            venues.Create(new Venue { Id = 12, City = cities.Get(2), Name = "Дворец культуры области", Address = "пр-т. Пушкина, 7" });


            venues.Create(new Venue { Id = 4, City = cities.Get(3), Name = "Центр культуры", Address = "ул. Дзержинского, 1" });
            venues.Create(new Venue { Id = 5, City = cities.Get(3), Name = "Драматический театр", Address = "ул.Мостовая, 35" });

            venues.Create(new Venue { Id = 6, City = cities.Get(4), Name = "Филармония", Address = "пл. Ленина, 69" });
            venues.Create(new Venue { Id = 7, City = cities.Get(4), Name = "VZAP", Address = "ул. Октябрьская, 2" });

            venues.Create(new Venue { Id = 8, City = cities.Get(5), Name = "Филармония", Address = "ул. Орджоникидзе, 14" });
            venues.Create(new Venue { Id = 9, City = cities.Get(5), Name = "Авеню", Address = "ул. Советская, 50" });

            venues.Create(new Venue { Id = 10, City = cities.Get(6), Name = "Гомельский ГЦК", Address = "ул. Ирининская, 16" });
            venues.Create(new Venue { Id = 11, City = cities.Get(6), Name = "Ледовый дворец", Address = "ул. Мазурова, 110" });
        }
        private static void AddEvents()
        {
            events.Create(new Event()
            {
                Id = 1,
                Name = "Новогодняя ярмарка чудес",
                Date = DateTimeGenerator(1, 16, 30),
                Description = "Музыкальная сказка для детей «Новогодняя ярмарка чудес» Автор проекта и режиссер - постановщик – Татьяна Можар, композитор – Владимир Савчик, балетмейстер – Марина Филатова",
                Venue = venues.Get(2),
                Banner = "YarmorkaChudes.png"
            });
            events.Create(new Event()
            {
                Id = 2,
                Name = "Алиса в стране чудес, спектакль на льду",
                Date = DateTimeGenerator(1, 18, 00),
                Venue = Venues.Get(11),
                Description = TextDescriptions.Event1,
                Banner = "AlisaVstraneChudes.jpg"
            });
            events.Create(new Event()
            {
                Id = 3,
                Name = "Концерт Виталия Сычева",
                Date = DateTimeGenerator(1, 21, 00),
                Venue = Venues.Get(10),
                Description = TextDescriptions.Event2,
                Banner = "LetSneg.jpg"
            });
            events.Create(new Event()
            {
                Id = 4,
                Name = "Концерт хора государственной академической симфонической капеллы России",
                Date = DateTimeGenerator(2, 21, 00),
                Venue = Venues.Get(2),
                Description = TextDescriptions.Event3,
                Banner = "HorOrk.png"
            });
            events.Create(new Event()
            {
                Id = 5,
                Name = "Концерт группы Контрабанда в ресторане «Друзья»",
                Date = DateTimeGenerator(2, 22, 00),
                Venue = Venues.Get(1),
                Description = TextDescriptions.Event4,
                Banner = "zhivaya-muzyka-655740.jpg"
            });
            events.Create(new Event()
            {
                Id = 6,
                Name = "Концерт Riga mem",
                Date = DateTimeGenerator(2, 15, 00),
                Venue = Venues.Get(6),
                Description = TextDescriptions.Event5,
                Banner = "0b6cb58166311f569e6f1acb2ad7e9ff.jpg"
            });
        }
        private static void AddUsers()
        {
            var normalizer = new UpperInvariantLookupNormalizer();
            User admin = new User
            {
                Id = 1,
                Login = normalizer.Normalize("Admin"),
                Role = Role.Admin,
                FirstName = "Sergey",
                LastName = "Sergeev",
                Adress = "gomel sity st 1",
                Localization = "ru-ru",
                PhoneNumber = "242331"
            };
            admin.Password = PasswordHasher.Hash("Admin");
            User user = new User
            {
                Id = 2,
                Login = normalizer.Normalize("User"),
                Role = Role.User,
                FirstName = "Andrey",
                LastName = "Andreev",
                Adress = "gomel sity st 21",
                Localization = "ru-ru",
                PhoneNumber = "254331"
            };
            user.Password = PasswordHasher.Hash("User");
            User seller = new User
            {
                Id = 3,
                Login = normalizer.Normalize("Seller"),
                Role = Role.User,
                FirstName = "Pavel",
                LastName = "Pavlov",
                Adress = "gomel sity st3 1",
                Localization = "ru-ru",
                PhoneNumber = "2434331"
            };
            seller.Password = PasswordHasher.Hash("Seller");

            users.Create(admin);
            users.Create(user);
            users.Create(seller);
        }
        private static void AddTickets()
        {
            tickets.Create(new Ticket() { Id = 1, Seller = users.Get(1), Event = events.Get(1), Price = 2000, Note = "Seller Notes simple" });
            tickets.Create(new Ticket() { Id = 2, Seller = users.Get(1), Event = events.Get(1), Price = 1000, Note = "Last row" });
            tickets.Create(new Ticket() { Id = 3, Seller = users.Get(2), Event = events.Get(2), Price = 2020, Note = "" });
            tickets.Create(new Ticket() { Id = 4, Seller = users.Get(2), Event = events.Get(2), Price = 3100, Note = "perfect" });
            tickets.Create(new Ticket() { Id = 5, Seller = users.Get(1), Event = events.Get(3), Price = 1230, Note = "" });
            tickets.Create(new Ticket() { Id = 6, Seller = users.Get(3), Event = events.Get(3), Price = 3210, Note = "Seller Notes simple" });
            tickets.Create(new Ticket() { Id = 7, Seller = users.Get(3), Event = events.Get(4), Price = 1230, Note = "dont buy this ticket pls" });
            tickets.Create(new Ticket() { Id = 8, Seller = users.Get(1), Event = events.Get(4), Price = 3210, Note = "Sel" });
            tickets.Create(new Ticket() { Id = 9, Seller = users.Get(1), Event = events.Get(5), Price = 1120, Note = "Stes simple" });
            tickets.Create(new Ticket() { Id = 10, Seller = users.Get(2), Event = events.Get(5), Price = 2300, Note = "Seller Notmple" });
            tickets.Create(new Ticket() { Id = 11, Seller = users.Get(2), Event = events.Get(6), Price = 2200, Note = "Sellemple" });
            tickets.Create(new Ticket() { Id = 12, Seller = users.Get(3), Event = events.Get(6), Price = 1400, Note = "Seller Notmple" });
            tickets.Create(new Ticket() { Id = 13, Seller = users.Get(1), Event = events.Get(1), Price = 1500, Note = "Seller Nes simple" });
            tickets.Create(new Ticket() { Id = 14, Seller = users.Get(3), Event = events.Get(2), Price = 1600, Note = "Selple" });
            tickets.Create(new Ticket() { Id = 15, Seller = users.Get(3), Event = events.Get(3), Price = 1660, Note = "Selple" });
            tickets.Create(new Ticket() { Id = 16, Seller = users.Get(2), Event = events.Get(4), Price = 1270, Note = "Seltes simple" });
            tickets.Create(new Ticket() { Id = 17, Seller = users.Get(2), Event = events.Get(5), Price = 1999, Note = "Seller Nomple" });
            tickets.Create(new Ticket() { Id = 18, Seller = users.Get(1), Event = events.Get(6), Price = 1099, Note = "Sellimple" });
            tickets.Create(new Ticket() { Id = 19, Seller = users.Get(1), Event = events.Get(1), Price = 3099, Note = "Seple" });
            tickets.Create(new Ticket() { Id = 20, Seller = users.Get(2), Event = events.Get(1), Price = 1800, Note = "Selltes sple" });
            tickets.Create(new Ticket() { Id = 21, Seller = users.Get(1), Event = events.Get(2), Price = 2000, Note = "Ser Notple" });
        }
        private static void AddOrders()
        {
            orders.Create(new Order() { Id = 1, Ticket = tickets.Get(1), Status = Status.Confirmed, Buyer = users.Get(2), TrackNumber = "1234567890" });
            orders.Create(new Order() { Id = 2, Ticket = tickets.Get(2), Status = Status.Rejected, Buyer = users.Get(3), TrackNumber = "111122223"  });
            orders.Create(new Order() { Id = 3, Ticket = tickets.Get(3), Status = Status.Rejected, Buyer = users.Get(3), TrackNumber = "RG62344535BY" });
            orders.Create(new Order() { Id = 4, Ticket = tickets.Get(8), Status = Status.Confirmed, Buyer = users.Get(2), TrackNumber = "RT85632420BY" });
            orders.Create(new Order() { Id = 5, Ticket = tickets.Get(4), Status = Status.Confirmed, Buyer = users.Get(1), TrackNumber = "RB23452220BY" });
            orders.Create(new Order() { Id = 6, Ticket = tickets.Get(5), Status = Status.Confirmed, Buyer = users.Get(2), TrackNumber = "RR27843232BY" });
            orders.Create(new Order() { Id = 7, Ticket = tickets.Get(11), Status = Status.Confirmed, Buyer = users.Get(1), TrackNumber = "RN09264243BY" });
            orders.Create(new Order() { Id = 8, Ticket = tickets.Get(13), Status = Status.WaitigConformation, Buyer = users.Get(2), TrackNumber = "RB83525753BY" });
            orders.Create(new Order() { Id = 9, Ticket = tickets.Get(15), Status = Status.Rejected, Buyer = users.Get(2), TrackNumber = "RG92352385BY" });
            orders.Create(new Order() { Id = 10, Ticket = tickets.Get(14), Status = Status.Rejected, Buyer = users.Get(2), TrackNumber = "RK46343845BY" });

        }

        private static DateTime DateTimeGenerator(int incDays, int hour, int min)
        {
            DateTime dt = DateTime.Now.AddDays(incDays);
            return new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, min, 0);
        }

    }
}
