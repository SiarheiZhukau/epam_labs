﻿using EpamLab.ResaleTickets.DataAccess.Entries;
using EpamLab.ResaleTickets.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EpamLab.ResaleTickets.DataAccess.Repositories.TestMemory
{
    public class MemoryTestRepository<T> : IRepository<T> where T : BaseEntries
    {
        private List<T> entities;
        public MemoryTestRepository(List<T> entities)
        {
            this.entities = entities;
        }
        public MemoryTestRepository()
        {
            this.entities = new List<T>();
        }
        public void Create(T item)
        {
            entities.Add(item);
        }

        public void Delete(int id)
        {
            T item = entities.Find(p => p.Id == id);
            if (item != null)
                entities.Remove(item);
        }

        public IEnumerable<T> Find(Func<T, bool> predicate)
        {
            return entities.Where(predicate).ToList();
        }

        public T Get(int id)
        {
            return entities.Find(p => p.Id == id);
        }

        public IEnumerable<T> GetAll()
        {
            return entities;
        }

        public void Update(T item)
        {
            Delete(item.Id);
            Create(item);
        }
    }
}
