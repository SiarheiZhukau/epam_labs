﻿using EpamLab.ResaleTickets.DataAccess.Entries;
using System;

namespace EpamLab.ResaleTickets.DataAccess.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<User> Users { get; }
        IRepository<Order> Orders { get; }
        IRepository<Event> Events { get; }
        IRepository<City> Cities { get; }
        IRepository<Ticket> Tickets { get; }
        IRepository<Venue> Venues { get; }
        void Save();
    }
}
