﻿using EpamLab.ResaleTickets.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using EpamLab.ResaleTickets.DataAccess.Entries;
using EpamLab.ResaleTickets.DataAccess.Repositories.TestMemory;

namespace EpamLab.ResaleTickets.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private IRepository<User> users = MemoryTestStorage.Users;
        private IRepository<City> cities = MemoryTestStorage.Cities;
        private IRepository<Venue> venues = MemoryTestStorage.Venues;
        private IRepository<Event> events = MemoryTestStorage.Events;
        private IRepository<Ticket> tickets = MemoryTestStorage.Tickets;
        private IRepository<Order> orders = MemoryTestStorage.Orders;

        public UnitOfWork()
        {
        }
        private void ListToRep<T>(List<T> list, IRepository<T> rep) where T : class
        {
            foreach(T r in list)
            {
                rep.Create(r);
            }
        }
        public IRepository<City> Cities
        {
            get
            {
                return cities;
            }
        }

        public IRepository<Event> Events
        {
            get
            {
                return events;
            }
        }

        public IRepository<Order> Orders
        {
            get
            {
                return orders;
            }
        }

        public IRepository<Ticket> Tickets
        {
            get
            {
                return tickets;
            }
        }

        public IRepository<User> Users
        {
            get
            {
                return users;
            }
        }

        public IRepository<Venue> Venues
        {
            get
            {
                return venues;
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}
