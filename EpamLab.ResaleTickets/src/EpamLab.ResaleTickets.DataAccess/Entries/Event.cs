﻿using System;

namespace EpamLab.ResaleTickets.DataAccess.Entries
{
    public class Event : BaseEntries
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public Venue Venue { get; set; }
        public string Banner { get; set; }
        public string Description { get; set; }
    }
}
