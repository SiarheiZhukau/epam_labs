﻿namespace EpamLab.ResaleTickets.DataAccess.Entries
{
    public class Ticket : BaseEntries
    {
        public Event Event { get; set; }
        public decimal Price { get; set; }
        public User Seller { get; set; }
        public string Note { get; set; }
    }
}
