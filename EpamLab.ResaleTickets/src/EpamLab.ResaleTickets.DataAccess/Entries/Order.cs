﻿namespace EpamLab.ResaleTickets.DataAccess.Entries
{
    public class Order : BaseEntries
    {
        public Status Status { get; set; }
        public Ticket Ticket { get; set; }
        public User Buyer { get; set; }
        public string TrackNumber { get; set; }
    }
}
