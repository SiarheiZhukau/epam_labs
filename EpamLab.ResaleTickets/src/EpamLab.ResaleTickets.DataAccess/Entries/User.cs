﻿namespace EpamLab.ResaleTickets.DataAccess.Entries
{
    public class User : BaseEntries
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Localization { get; set; }
        public string Adress { get; set; }
        public string PhoneNumber { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public Role Role { get; set; }
    }
}
