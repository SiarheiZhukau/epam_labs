﻿namespace EpamLab.ResaleTickets.DataAccess.Entries
{
    public class Venue : BaseEntries
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public City City { get; set; }
    }
}
