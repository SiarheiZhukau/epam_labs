﻿namespace EpamLab.ResaleTickets.DataAccess.Entries
{
    public class City : BaseEntries
    {
        public string Name { get; set; }
    }
}
