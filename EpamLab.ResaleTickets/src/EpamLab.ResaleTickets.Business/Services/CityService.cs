﻿using EpamLab.ResaleTickets.Business.Interfaces;
using System.Collections.Generic;
using EpamLab.ResaleTickets.Business.Models;
using EpamLab.ResaleTickets.DataAccess.Interfaces;
using AutoMapper;
using EpamLab.ResaleTickets.DataAccess.Entries;

namespace EpamLab.ResaleTickets.Business.Services
{
    public class CityService : ICityService
    {
        IUnitOfWork uow;
        IMapper mapper;
        public CityService(IUnitOfWork uow, IMapper mapper)
        {
            this.uow = uow;
            this.mapper = mapper;
        }

        public CityBusinessModel Get(int id)
        {
            return mapper.Map<City, CityBusinessModel>(uow.Cities.Get(id));
        }

        public IEnumerable<CityBusinessModel> GetAll()
        {
            return mapper.Map<IEnumerable<City>, IEnumerable<CityBusinessModel>>(uow.Cities.GetAll());
        }
    }
}
