﻿using EpamLab.ResaleTickets.Business.Interfaces;
using System.Collections.Generic;
using System.Linq;
using EpamLab.ResaleTickets.Business.Models;
using EpamLab.ResaleTickets.DataAccess.Interfaces;
using AutoMapper;
using EpamLab.ResaleTickets.DataAccess.Entries;

namespace EpamLab.ResaleTickets.Business.Services
{
    public class UserService : IUserService
    {
        IUnitOfWork uow;
        IMapper mapper;

        public UserService(IUnitOfWork uow, IMapper mapper)
        {
            this.uow = uow;
            this.mapper = mapper;
        }
        public UserBusinessModel Get(int id)
        {
            return mapper.Map<User, UserBusinessModel>(uow.Users.Get(id));
        }

        public IEnumerable<UserBusinessModel> GetAll()
        {
            return mapper.Map<IEnumerable<User>, IEnumerable<UserBusinessModel>>(uow.Users.GetAll());
        }

        public UserBusinessModel Aunthefication(string normalizeLogin, string hashPassword)
        {
            return mapper.Map<User, UserBusinessModel>(uow.Users.Find(u => u.Login == normalizeLogin && u.Password == hashPassword).First());
        }
    }
}
