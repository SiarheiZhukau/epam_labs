﻿using EpamLab.ResaleTickets.Business.Interfaces;
using System;
using System.Collections.Generic;
using EpamLab.ResaleTickets.Business.Models;
using EpamLab.ResaleTickets.DataAccess.Interfaces;
using AutoMapper;
using EpamLab.ResaleTickets.DataAccess.Entries;

namespace EpamLab.ResaleTickets.Business.Services
{
    public class EventService : IEventService
    {
        IUnitOfWork uow;
        IMapper mapper;
        public EventService(IUnitOfWork uow, IMapper mapper)
        {
            this.uow = uow;
            this.mapper = mapper;
        }

        public EventBusinessModel Get(int Id)
        {
            return mapper.Map<Event, EventBusinessModel>(uow.Events.Get(Id));
        }
        public IEnumerable<EventBusinessModel> GetActual()
        {
            return mapper.Map<IEnumerable<Event>, IEnumerable<EventBusinessModel>>(uow.Events.Find(e => e.Date > DateTime.Today));
        }
        public IEnumerable<EventBusinessModel> GetIntervalDates(DateTime begin, DateTime end)
        {
            if (begin > end)
            {
                return null;
            }
            return mapper.Map<IEnumerable<Event>, IEnumerable<EventBusinessModel>>(uow.Events.Find(e => e.Date >= begin && e.Date <= end));
        }
        public IEnumerable<EventBusinessModel> GetAll()
        {
            return mapper.Map<IEnumerable<Event>, IEnumerable<EventBusinessModel>>(uow.Events.GetAll());
        }
    }
}
