﻿using EpamLab.ResaleTickets.Business.Interfaces;
using System.Collections.Generic;
using System.Linq;
using EpamLab.ResaleTickets.Business.Models;
using EpamLab.ResaleTickets.DataAccess.Interfaces;
using AutoMapper;
using EpamLab.ResaleTickets.DataAccess.Entries;

namespace EpamLab.ResaleTickets.Business.Services
{
    public class TicketService : ITicketService
    {
        IUnitOfWork uow;
        IMapper mapper;
        public TicketService(IUnitOfWork uow, IMapper mapper)
        {
            this.uow = uow;
            this.mapper = mapper;
        }

        public TicketBusinessModel Get(int id)
        {
            return mapper.Map<Ticket, TicketBusinessModel>(uow.Tickets.Get(id));
        }

        public IEnumerable<TicketBusinessModel> GetAll()
        {
            return mapper.Map<IEnumerable<Ticket>, IEnumerable<TicketBusinessModel>>(uow.Tickets.GetAll());
        }

        public IEnumerable<TicketBusinessModel> GetForEvent(int id)
        {
            IEnumerable<TicketBusinessModel> tickets = mapper.Map<IEnumerable<Ticket>, IEnumerable<TicketBusinessModel>>(uow.Tickets.GetAll());
            IEnumerable<TicketBusinessModel> ticketsForEvent = tickets.Where(t => t.Event.Id == id).ToList<TicketBusinessModel>();
            return ticketsForEvent;
        }

        public IEnumerable<TicketBusinessModel> GetFromUser(int id)
        {
            IEnumerable<TicketBusinessModel> tickets = mapper.Map<IEnumerable<Ticket>, IEnumerable<TicketBusinessModel>>(uow.Tickets.GetAll());
            IEnumerable<TicketBusinessModel> ticketsFromUser = tickets.Where(t => t.Seller.Id == id).ToList<TicketBusinessModel>();
            return ticketsFromUser;
        }

        public IEnumerable<TicketBusinessModel> GetSoldFromUser(int id)
        {
            OrderService orderServices = new OrderService(uow,mapper);
            IEnumerable<OrderBusinessModel> orders = orderServices.GetSellerIsConfirmed(id);
            return OrdersToTickets(orders);
        }
        public IEnumerable<TicketBusinessModel> GetWaitingFromUser(int id)
        {
            OrderService orderServices = new OrderService(uow, mapper);
            IEnumerable<OrderBusinessModel> orders = orderServices.GetSellerIsWaiting(id);
            return OrdersToTickets(orders);
        }

        private IEnumerable<TicketBusinessModel> OrdersToTickets(IEnumerable<OrderBusinessModel> orders)
        {
            List<TicketBusinessModel> tickets = new List<TicketBusinessModel>();
            foreach (OrderBusinessModel o in orders)
            {
                tickets.Add(o.Ticket);
            }
            return tickets;
        }
    }
}
