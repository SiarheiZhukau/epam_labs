﻿using EpamLab.ResaleTickets.Business.Interfaces;
using System.Collections.Generic;
using EpamLab.ResaleTickets.Business.Models;
using EpamLab.ResaleTickets.DataAccess.Entries;
using EpamLab.ResaleTickets.DataAccess.Interfaces;
using AutoMapper;

namespace EpamLab.ResaleTickets.Business.Services
{
    public class VenueService : IVenueService
    {
        IUnitOfWork uow;
        IMapper mapper;
        public VenueService(IUnitOfWork uow, IMapper mapper)
        {
            this.uow = uow;
            this.mapper = mapper;
        }
        public VenueBusinessModel Get(int id)
        {
            return mapper.Map<Venue, VenueBusinessModel>(uow.Venues.Get(id));
        }

        public IEnumerable<VenueBusinessModel> GetAll()
        {
            return mapper.Map<IEnumerable<Venue>,IEnumerable<VenueBusinessModel>>(uow.Venues.GetAll());
        }
    }
}
