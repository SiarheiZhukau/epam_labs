﻿using EpamLab.ResaleTickets.Business.Interfaces;
using System.Collections.Generic;
using System.Linq;
using EpamLab.ResaleTickets.Business.Models;
using EpamLab.ResaleTickets.DataAccess.Interfaces;
using AutoMapper;
using EpamLab.ResaleTickets.DataAccess.Entries;

namespace EpamLab.ResaleTickets.Business.Services
{
    public class OrderService : IOrderService
    {
        
        IUnitOfWork uow;
        IMapper mapper;
        public OrderService(IUnitOfWork uow, IMapper mapper)
        {
            this.uow = uow;
            this.mapper = mapper;
            
        }

        public OrderBusinessModel Get(int id)
        {
            return mapper.Map<Order, OrderBusinessModel>(uow.Orders.Get(id));
        }
        
        public IEnumerable<OrderBusinessModel> GetAll()
        {
            return mapper.Map<IEnumerable<Order>, IEnumerable<OrderBusinessModel>>(uow.Orders.GetAll());
        }

        public IEnumerable<OrderBusinessModel> GetBuyer(int id)
        {
            IEnumerable<OrderBusinessModel> ordersBuyer =
                 GetAll().Where(x => x.Buyer.Id == id).ToList();
            return ordersBuyer;
        }

        public IEnumerable<OrderBusinessModel> GetBuyerIsConfirmed(int id)
        {
            IEnumerable<OrderBusinessModel> ordersBuyerIsConfirmed =
                GetAll().Where(ord => ord.Status == StatusBusinessModel.Confirmed && ord.Buyer.Id == id).ToList();
            return ordersBuyerIsConfirmed;
        }

        public IEnumerable<OrderBusinessModel> GetBuyerIsWaiting(int id)
        {
            IEnumerable<OrderBusinessModel> ordersBuyerIsWaiting =
                GetAll().Where(ord => ord.Status == StatusBusinessModel.WaitigConformation && ord.Buyer.Id == id).ToList();
            return ordersBuyerIsWaiting;
        }

        public IEnumerable<OrderBusinessModel> GetSellerIsConfirmed(int id)
        {
            IEnumerable<OrderBusinessModel> ordersSellerIsWaiting =
                GetAll().Where(ord => ord.Status == StatusBusinessModel.Confirmed && ord.Ticket.Seller.Id == id).ToList();
            return ordersSellerIsWaiting;
        }

        public IEnumerable<OrderBusinessModel> GetSellerIsWaiting(int id)
        {
            IEnumerable<OrderBusinessModel> ordersSellerIsWaiting =
                GetAll().Where(ord => ord.Status == StatusBusinessModel.WaitigConformation && ord.Ticket.Seller.Id == id).ToList();
            return ordersSellerIsWaiting;
        }

        public IEnumerable<OrderBusinessModel> GetBuyer(string login)
        {
            IEnumerable<OrderBusinessModel> ordersBuyer =
                  GetAll().Where(x => x.Buyer.Login == login).ToList();
            return ordersBuyer;
        }
    }
}
