﻿using EpamLab.ResaleTickets.Business.Interfaces;
using EpamLab.ResaleTickets.Business.Services;
using Microsoft.Extensions.DependencyInjection;

namespace EpamLab.ResaleTickets.Business
{
    public static class DependencyInjector
    {
        public static IServiceCollection AddDataServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IOrderService, OrderService>();
            serviceCollection.AddScoped<IEventService, EventService>();
            serviceCollection.AddScoped<ITicketService, TicketService>();
            serviceCollection.AddScoped<ICityService, CityService>();
            serviceCollection.AddScoped<IVenueService, VenueService>();
            serviceCollection.AddScoped<IUserService, UserService>();

            return serviceCollection;
        }
    }
}
