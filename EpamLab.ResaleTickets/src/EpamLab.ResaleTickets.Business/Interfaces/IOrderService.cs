﻿using EpamLab.ResaleTickets.Business.Models;
using System.Collections.Generic;

namespace EpamLab.ResaleTickets.Business.Interfaces
{
    public interface IOrderService : IEntityService<OrderBusinessModel>
    {
        IEnumerable<OrderBusinessModel> GetSellerIsWaiting(int id);
        IEnumerable<OrderBusinessModel> GetSellerIsConfirmed(int id);
        IEnumerable<OrderBusinessModel> GetBuyer(int id);
        IEnumerable<OrderBusinessModel> GetBuyer(string login);
        IEnumerable<OrderBusinessModel> GetBuyerIsWaiting(int id);
        IEnumerable<OrderBusinessModel> GetBuyerIsConfirmed(int id);
    }
}
