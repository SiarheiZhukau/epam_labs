﻿using EpamLab.ResaleTickets.Business.Models;

namespace EpamLab.ResaleTickets.Business.Interfaces
{
    public interface ICityService : IEntityService<CityBusinessModel>
    {
    }
}
