﻿using System.Collections.Generic;

namespace EpamLab.ResaleTickets.Business.Interfaces
{
    public interface IEntityService<TEntity>
                                where TEntity : class
    {
        TEntity Get(int id);
        IEnumerable<TEntity> GetAll();
    }
}
