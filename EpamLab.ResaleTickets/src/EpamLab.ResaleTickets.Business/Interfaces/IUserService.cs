﻿using EpamLab.ResaleTickets.Business.Models;

namespace EpamLab.ResaleTickets.Business.Interfaces
{
    public interface IUserService : IEntityService<UserBusinessModel>
    {
        UserBusinessModel Aunthefication(string normalizeLogin, string hashPassword);
    }
}
