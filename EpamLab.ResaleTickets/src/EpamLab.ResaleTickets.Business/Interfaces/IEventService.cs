﻿using EpamLab.ResaleTickets.Business.Models;
using System;
using System.Collections.Generic;

namespace EpamLab.ResaleTickets.Business.Interfaces
{
    public interface IEventService : IEntityService<EventBusinessModel>
    {
        IEnumerable<EventBusinessModel> GetIntervalDates(DateTime begin, DateTime end);
        IEnumerable<EventBusinessModel> GetActual();
    }
}
