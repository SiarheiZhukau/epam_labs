﻿using EpamLab.ResaleTickets.Business.Models;
using System.Collections.Generic;

namespace EpamLab.ResaleTickets.Business.Interfaces
{
    public interface ITicketService : IEntityService<TicketBusinessModel>
    {
        IEnumerable<TicketBusinessModel> GetForEvent(int id);
        IEnumerable<TicketBusinessModel> GetFromUser(int id);
        IEnumerable<TicketBusinessModel> GetWaitingFromUser(int id);
        IEnumerable<TicketBusinessModel> GetSoldFromUser(int id);
    }
}
