﻿namespace EpamLab.ResaleTickets.Business.Models
{
    public class TicketBusinessModel
    {
        public int Id { get; set; }
        public EventBusinessModel Event { get; set; }
        public decimal Price { get; set; }
        public UserBusinessModel Seller { get; set; }
        public string Note { get; set; }
    }
}
