﻿namespace EpamLab.ResaleTickets.Business.Models
{
    public class OrderBusinessModel
    {
        public int Id { get; set; }
        public StatusBusinessModel Status { get; set; }
        public TicketBusinessModel Ticket { get; set; }
        public UserBusinessModel Buyer { get; set; }
        public string TrackNumber { get; set; }
    }
}
