﻿namespace EpamLab.ResaleTickets.Business.Models
{
    public class VenueBusinessModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public CityBusinessModel City { get; set; }
    }
}
