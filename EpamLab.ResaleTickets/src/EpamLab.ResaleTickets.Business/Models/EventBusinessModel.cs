﻿using System;

namespace EpamLab.ResaleTickets.Business.Models
{
    public class EventBusinessModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public VenueBusinessModel Venue { get; set; }
        public string Banner { get; set; }
        public string Description { get; set; }
    }
}
