﻿namespace EpamLab.ResaleTickets.Business.Models
{
    public class CityBusinessModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
